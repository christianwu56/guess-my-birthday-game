from random import randint

User_Name = input("Hey! What is your name? ")
counter = 1

for i in range(5):
    month_num = randint(1,12)
    year_num = randint(1924,2004)
    print("Guess" , counter,  ": " , User_Name + " were you born on " , month_num , "/" , year_num , "? ")
    user_input = input(" yes or no? ")
    counter += 1
    if user_input == "no":
        print("Drat! Lemme try again! ")
        if counter == 6:
            print("Too many tries you lose! ")
            exit()
    else:
        print("I knew it!")
        exit()
